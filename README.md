# ansible-laravel

## Run playbook
`ansible_user` can be either `ubuntu` or `demo`
`ansible_ssh_private_key_file` must be provided
Prepare inventory `.ini` file in the next format:
```
[DEFAULT]
hostname
[DEFAULT:vars]
ansible_ssh_private_key_file = <YOUR private key>
ansible_user = # ubuntu or demo
```

Command to run% 
```
ansible-playbook -i inventory.ini site.yml --become
```

## Answers
Answers are located [here](docs/test_answers.pdf).